const student = {
  name: prompt('Enter your name'),
  lastName: prompt('Enter your surname'),
  tabel: {
    Maths: null,
    English: null,
    Ukrainian: null,
    Physics: null,
    Chemistry: null,
    Geography: null,
    Biology: null,
    History: null,
    Literature: null,
    Music: null,
  },
};
for (let n in student.tabel) {
  student.tabel[n] = prompt(`What's your score in ${n}?`);
  if (student.tabel[n] === null) {
    break;
  }
}
let badScore = 0;
let sumScore = 0;
for (let key in student.tabel) {
  sumScore += +student.tabel[key];
  if (student.tabel[key] === null) {
    delete student.tabel[key];
  }
  if (+student.tabel[key] < 4) {
    badScore += 1;
  }
}
if (badScore === 0) {
  alert(
    "You don't have any bad scores! You are transfered to the next course!"
  );
}
const averageScore = +(sumScore / Object.keys(student.tabel).length).toFixed(2);
if (averageScore > 7) {
  alert(
    `Your average score is: ${averageScore}. You are awarded a scholarship!`
  );
}
